{-# LANGUAGE ScopedTypeVariables, FlexibleContexts #-}

module Random
    ( prandom,
      generateRandoms,
      metropolis
    ) where


import Control.Lens
import Data.Array.Accelerate as A
import Data.Array.Accelerate.Data.Bits (xor,shiftR,(.&.))


murmur_m :: Exp Word64
murmur_m = constant 0xc6a4a7935bd1e995

murmur_r :: Exp Int
murmur_r = constant 47

murmur :: Exp Word64 -> Exp Int -> Exp Word64
murmur seed index =
  let key = A.fromIntegral $ index + 1
      k1 = key * murmur_m
      k2 = k1 `xor` (k1 `shiftR` murmur_r)
      k3 = k2 * murmur_m
      h1 = seed * murmur_m
      h2 = (h1 `xor` k3) * murmur_m
  in h2

randFromSeed :: (Slice ix, Shape ix) => Exp Word64 -> Exp ix -> Exp ix -> Exp Word64
randFromSeed seed shape = murmur seed . toIndex shape

class PseudoRandom a where
  prandom :: (Slice ix, Shape ix) => Exp Word64 -> Exp ix -> Exp ix -> Exp a

instance PseudoRandom Word64 where
  prandom = randFromSeed

instance PseudoRandom Word32 where
  prandom seed shape = A.fromIntegral . (.&. (0xffffffff :: Exp Word64)) . prandom seed shape

instance PseudoRandom Word16 where
  prandom seed shape = A.fromIntegral . (.&. (0xffff :: Exp Word64)) . prandom seed shape

instance PseudoRandom Word8 where
  prandom seed shape = A.fromIntegral . (.&. (0xff :: Exp Word64)) . prandom seed shape

instance PseudoRandom Double where
  prandom seed shape loc =  (\x -> 1.0 - A.fromIntegral x / A.fromIntegral (constant 0x7fffffffffffffff :: Exp Word64)) $ (prandom seed shape loc :: Exp Word64)

instance PseudoRandom Float where
  prandom seed shape idx =  (\x -> 1.0 - (A.fromIntegral x) / A.fromIntegral (constant 0x7fffffff :: Exp Word32)) $ (prandom seed shape idx :: Exp Word32)

generateRandoms :: (Elt a, Slice ix, Shape ix, PseudoRandom b, Elt b) => Exp Word64 -> Acc (Array ix a) -> Acc (Array ix b)
generateRandoms seed arr = generate (shape arr) $ prandom seed (shape arr)



metropolisStep :: (Shape ix, Slice ix) => (Exp Double -> Exp Double) ->
  Exp Word64 ->
  Acc (Array ix (Int, Int, Double)) ->
  Acc (Array ix (Int, Int, Double))
metropolisStep f seed old = A.map g old
  where
    g step = step & lftN . _1 +~ 1
                  & lftN . _2 +~ 2 * shapeSize (shape old)
                  & lftN . _3 %~ update
      where
        update val = fudge * f val A.<* f (val + 5*next) ? (val + 5 * next, val)
        fudge = A.fromIntegral (murmur seed $ step ^. lftN . _2) / A.fromIntegral (constant 0xffffffffffffffff :: Exp Word64) :: Exp Double
        next = 1 - A.fromIntegral (murmur seed $ (step ^. lftN . _2) + shapeSize (shape old)) / A.fromIntegral (constant 0x7fffffffffffffff :: Exp Word64) :: Exp Double


metropolis :: (Shape ix, Slice ix) =>
  Exp Word64 ->
  (Exp Double -> Exp Double) ->
  Exp ix -> Acc (Array ix Double)
metropolis seed f sh = do
  let rs = prandom seed sh
  let base = A.zip3 (fill sh $ constant 0) (generate sh (\x -> toIndex sh x)) (generate sh rs)
  A.map (^. lftN . _3) $ awhile test (metropolisStep f $ seed) base
  where
    test = A.all $ (A.<* constant 20) . (^. lftN . _1)

lftN :: Lens' (Exp (Int, Int, Double)) (Exp Int, Exp Int, Exp Double)
lftN = iso unlift lift
