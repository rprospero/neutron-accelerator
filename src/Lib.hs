{-# LANGUAGE TypeOperators, MultiParamTypeClasses, TypeFamilies, FlexibleContexts, FlexibleInstances #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Lib
    ( someFunc,
      source,
      generateRandomPositions,
      detectorDivergence,
      Neutron,
      Wavelength,
      precessor
    ) where

import Control.Lens
import Data.Array.Accelerate as A
import Data.Array.Accelerate.Linear.Metric (dot, norm, normalize)
import Data.Array.Accelerate.Linear.V1
import Data.Array.Accelerate.Linear.V2 hiding (angle)
import Data.Array.Accelerate.Linear.V3
import Data.Array.Accelerate.Linear.Vector ((*^))
import Random

type Position = V3 Double
type Direction = V2 Double
type Wavelength = Double
type Neutron = (Position, Direction, Wavelength, Direction)
type ExpNeutron = (Exp Position, Exp Direction, Exp Wavelength, Exp Direction)


lftN :: Lens' (Exp Neutron) ExpNeutron
lftN = iso unlift lift

pos :: Lens' (Exp Neutron) (Exp Position)
pos = lftN . _1 

dir :: Lens' (Exp Neutron) (Exp Direction)
dir = lftN . _2

lam :: Lens' (Exp Neutron) (Exp Wavelength)
lam = lftN . _3

spin :: Lens' (Exp Neutron) (Exp Direction)
spin = lftN . _4

_theta :: Lens' (Exp Direction) (Exp Wavelength)
_theta = _x

_phi :: Lens' (Exp Direction) (Exp Wavelength)
_phi = _y

spherical :: Iso' (Exp Position) (Exp Direction)
spherical = iso getter setter
  where
    setter d =
      let
        theta = d ^. _theta
        phi = d ^. _phi
      in
        lift $ V3 (sin theta * sin phi) (sin theta * cos phi) (cos theta)
    getter p =
      let
        x = p ^. _x
        y = p ^. _y
        z = p ^. _z
        r = norm p
      in
        lift $ V2 (acos $ z/r) (A.atan2 y x)

cartesian :: Iso' (Exp Direction) (Exp Position)
cartesian = from spherical

detectorPosition :: Exp Neutron -> Exp Direction
detectorPosition n = n ^. pos . _xy

detectorDivergence :: Exp Neutron -> Exp Direction
detectorDivergence n = n ^. dir


getQ :: Exp Neutron -> Exp Position
getQ n =
  let
    l = n ^. lam
    k = 2 * pi / l
  in
    k *^ (n ^. (dir . cartesian))

setQ :: Exp Position -> Exp Neutron -> Exp Neutron
setQ q n = n & (dir . cartesian) .~ q

setLength :: Exp Wavelength -> Exp Position -> Exp Position
setLength l p = l *^ normalize p

offAngle :: Exp Wavelength -> Exp Wavelength -> Exp Position -> Exp Position
offAngle theta phi n = keep + change
  where
    keep = setLength (norm n * cos theta) n
    change = setLength (norm n * sin theta) $ rotAround n (cross (constant $ V3 1 0 0) n) phi

rotAround :: Exp Position -> Exp Position -> Exp Wavelength -> Exp Position
rotAround axis' v phi = a + b + c
  where
    axis = setLength 1 axis'
    a = (cos phi) *^ v :: Exp Position
    b = (sin phi) *^ (cross axis v) :: Exp Position
    c = ((axis `dot` v) * (1 - cos phi)) *^ axis :: Exp Position

scatterSample :: Exp Wavelength -> Exp Wavelength -> Exp Neutron -> Exp Neutron
scatterSample phi theta n = setQ (offAngle theta phi $ getQ n) n


someFunc :: Acc (Scalar Word64) -> Acc (Vector  Neutron) -> Acc (Array DIM2 (Int))
-- someFunc arr = A.zipWith advance (generateRandoms 0 arr) arr
someFunc aseed arr =
  let
    seed = A.the aseed
    angles = generateRandoms seed arr
    mets = metropolis (seed + (A.fromIntegral $ size arr)) ((\x -> ((sin x - x * cos x)/x**3)**2) . (*40)) (shape arr)
    adjust = roundVec . (+ (constant (V2 500 500))) . (20 *^) :: Exp (V2 Double) -> Exp (V2 Int)
    clamp = A.max 0 . A.min 999
    clamp2 v = lift $ V2 (clamp $ v ^. _x) (clamp $ v ^. _y)
  in
    hist2D (constant $ V2 1000 1000) . A.map (clamp2 . adjust . detectorPosition) . mask (seiveSpacedX 10 {-seive2d (0,0) (16,10)-})  . A.map (advance $ constant 10)$ A.zipWith3 scatterSample (A.map (* (2 * pi)) angles) mets arr

type Mask = Exp Neutron -> Exp Bool

mask :: Mask -> Acc (Vector Neutron) -> Acc (Vector Neutron)
mask seive = A.filter seive

seiveInvert :: Mask -> Mask
seiveInvert f = A.not . f

seiveRadial :: Double -> Mask
seiveRadial r n =
  (n ^. pos._x) ** 2 + (n ^. pos._y) ** 2 A.>* constant r**2

seive2d :: (Double, Double) -> (Double, Double) -> Mask
seive2d (x, y) (width, height) n =
  n ^. pos._x A.<* constant (x+width/2) &&*
  n ^. pos._x A.>* constant (x-width/2) &&*
  n ^. pos._y A.<* constant (y+height/2) &&*
  n ^. pos._y A.>* constant (y-height/2)

seiveSpacedX :: Double -> Mask
seiveSpacedX s n =
  A.round (n ^. pos._x / constant s) `A.mod` constant (2 :: Int) A.==* constant 0 

roundVec :: Exp (V2 Double) -> Exp (V2 Int)
roundVec v = lift $ V2 (A.round $ v ^. _x) (A.round $ v ^. _y)


generateRandomPositions :: (Elt a, Slice ix, Shape ix) => Exp Word64 -> Acc (Array ix a) -> Acc (Array ix Position)
generateRandomPositions seed arr = let xs = generateRandoms seed arr
                                       ys = generateRandoms seed arr
                                       zs = generateRandoms seed arr
                              in
                                A.zipWith3 (\x y z -> lift $ V3 x y z) xs ys zs

advance :: Exp Double -> Exp Neutron -> Exp Neutron
advance d n = n & (pos . _z) +~ d &
  (pos . _x) +~ d * tan theta * cos phi &
  (pos . _y) +~ d * tan theta * sin phi
  where
    theta = n ^. dir . _theta
    phi = n ^. dir . _phi

source :: (Shape ix, Slice ix) => (ix :. Int) -> Acc (Array (ix :. Int) Neutron)
source sh = generate (constant sh) f
  where
    f _ = lift $ constant (V3 0.0 0.0 0.0,
                           V2 0 0, 1, V2 0 0)

hist2D' :: Exp (Int, Int) -> Acc (Vector (Int, Int)) -> Acc (Array DIM2 (Int, Int))
hist2D' bins xs = permute
  (\a b -> lift (A.fst a + A.fst b, A.snd b))
  orig perm xs
  where
    orig = fill (A.uncurry index2 bins) (constant base) :: Acc (Array DIM2 (Int, Int))
    perm = A.uncurry index2 . (xs A.!) :: Exp DIM1 -> Exp DIM2
    base = (0, 0) :: (Int, Int)

hist2D :: Exp (V2 Int) -> Acc (Vector (V2 Int)) -> Acc (Array DIM2 Int)
hist2D bins xs = A.map A.fst $ hist2D' bins' $ A.map f xs
  where
    f = convert :: Exp (V2 Int) -> Exp (Int, Int)
    bins' = convert bins:: Exp (Int, Int)

convert :: Exp (V2 Int) -> Exp (Int, Int)
convert v = 
  let
    x = v ^. _x
    y = v ^. _y
  in
    lift (x,y)

-- | courtesy of http://www.physics.nist.gov/cgi-bin/cuu/Value?gamman
-- given in 1/(s T)
neutronGyromagneticMoment :: Double
neutronGyromagneticMoment = 1.83247172e8

-- | courtesy of http://www.physics.nist.gov/cgi-bin/cuu/Value?mn|search_for=neutron+mass
-- mass currentl in kg
neutronMass :: Double
neutronMass = 1.674927471e-27

-- | courtesy of http://www.physics.nist.gov/cgi-bin/cuu/Value?h|search_for=planck+constant
-- units of J s
planck :: Double
planck = 6.626070040e-34

_velocity :: Lens' (Exp Neutron) (Exp Double)
_velocity = lam . iso ((constant planck / constant neutronMass) /) ((constant planck /) . (* constant neutronMass))

precessor :: Exp Double -> Exp Direction -> Exp (V3 Double) -> Exp Neutron -> Exp Neutron
precessor distance angle field n =
  let
    omega = constant neutronGyromagneticMoment * norm field
    precess = omega * time
    time = realdist / n ^. _velocity
    m = n ^. dir . cartesian
    planeNorm = angle ^. cartesian
    displacement = lift $ V3 0 0 distance
    realdist = norm m * (displacement `dot` planeNorm) / (m `dot` planeNorm)
  in
    n & spin.cartesian %~ flip (rotAround field) precess

