{-# LANGUAGE Rank2Types #-}

module Unit where

import Control.Lens

data Length = Length
data Time = Time
data Mass = Mass
data Current = Current
data Product a b = Product a b
data Quotient a b = Quotient a b
data Unitless = UnitLess

data Physical u x = Physical x
                  deriving (Show, Read, Eq)

type Area = Product Length Length
type Volume = Product Length Area
type Speed = Quotient Length Time
type Acceleration = Quotient Speed Time
type Momentum = Product Mass Speed
type Force = Product Mass Acceleration
type Energy = Product Force Length
type Power = Quotient Energy Time
type Pressure = Product Force Area
type Charge = Product Current Time
type Voltage = Quotient Energy Charge
type EMF = Quotient Voltage Length
type MagneticField = Quotient (Product Voltage Time) Area

type Unit u x = Iso' x (Physical u x)
type Prefix u x = Iso' (Physical u x) (Physical u x)

fromPhysical :: Physical u x -> x
fromPhysical (Physical x) = x

_physical :: Unit u x
_physical = iso Physical fromPhysical

_meter :: Unit Length x
_meter = _physical

_second :: Unit Time x
_second = _physical

_hour :: (Fractional x) => Unit Time x
_hour = iso (/3600) (*3600) . _physical

_gram :: (Fractional x) => Unit Mass x
_gram = (iso (*1000) (/1000)) . _physical

prefixer :: (Fractional x) => x -> Prefix u x
prefixer n = from _physical . iso (*n) (/n) . _physical

_deci :: (Fractional x) => Prefix u x
_deci = prefixer 10

_centi :: (Fractional x) => Prefix u x
_centi = prefixer 100

_milli :: (Fractional x) => Prefix u x
_milli = prefixer 1000

_micro :: (Fractional x) => Prefix u x
_micro = prefixer 1000000

_nano :: (Fractional x) => Prefix u x
_nano = prefixer 1000000000

_deca :: (Fractional x) => Prefix u x
_deca = from _deci

_kilo :: (Fractional x) => Prefix u x
_kilo = from _milli

_mega :: (Fractional x) => Prefix u x
_mega = from _micro

_giga :: (Fractional x) => Prefix u x
_giga = from _nano

isoProd :: Iso' x (Physical u1 x) -> Iso' x (Physical u2 x) -> Iso' x (Physical (Product u1 u2) x)
isoProd a b = iso getter setter
  where
    getter x = x ^. _physical . from a . _physical . from b . _physical
    setter x = x ^. from _physical . a . from _physical  . b . from _physical

isoQuot :: Iso' x (Physical u1 x) -> Iso' x (Physical u2 x) -> Iso' x (Physical (Quotient u1 u2) x)
isoQuot a b = iso getter setter
  where
    getter x = x ^. _physical . from a . b . from _physical . _physical
    setter x = x ^. from _physical . a . from _physical . _physical . from b

_meterSquare :: Unit Area x
_meterSquare = isoProd _meter _meter

_cm :: (Fractional x) => Unit Length x
_cm = _meter . _centi

_angstrom :: (Fractional x) => Unit Length x
_angstrom = _meter . (_centi . _deci)

_cm2 :: (Fractional x) => Unit Area x
_cm2 = isoProd _cm _cm

_mPs :: Unit Speed x
_mPs = isoQuot _meter _second

_mPh :: (Fractional x) => Unit Speed x
_mPh = isoQuot _meter _hour
