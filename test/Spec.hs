{-# LANGUAGE OverloadedStrings #-}
import Control.Lens
import Test.Hspec
import Unit

main :: IO ()
main = hspec mySpec

mySpec :: SpecWith ()
mySpec = describe "everything" $ do
  unitSpec

unitSpec :: SpecWith ()
unitSpec = describe "unit library" $ do
  it "test centi meter" $ (100.0 :: Double) == ((1.0 :: Double) ^. _meter) ^. _centi . from _meter
  it "adds" $ 1.01 == (((1.0 :: Double) ^. _meter)& _centi . from _meter +~ 1) ^. from _meter
  it "square" $ 1e4 == ((1.0 :: Double) ^. _meterSquare) ^. from _cm2
  it "quot" $ 3600 == ((1.0 :: Double) ^. _mPs) ^. from _mPh
