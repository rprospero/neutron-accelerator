module Main where

import Codec.Picture
import Data.Array as DA
import qualified Data.Array.Accelerate as A
-- import Data.Array.Accelerate.Interpreter as Interp
import Data.Array.Accelerate.CUDA as CUDA
import qualified Data.Foldable as F
import Lib
import Linear.V3
import System.Random (newStdGen, randoms)

runner :: (A.Arrays a, A.Arrays b) => (A.Acc a -> A.Acc b) -> a -> b
runner = CUDA.run1

getR :: Neutron -> Wavelength
getR (V3 x y z,_,_,_) = atan2 (sqrt $ x*x+y*y) z

-- histogram :: Int -> Int -> (Double, Double) -> (Double, Double) -> A.Vector (V2 Double) -> DA.Array (Int,Int) Float
-- histogram xBins yBins (xmin, xmax) (ymin, ymax) = accumArray (+) 0 bnds  . Prelude.map f . A.toList
--   where
--     bnds = ((0, 0), (xBins, yBins))
--     f (V2 x y) = ((max 0 . min (xBins - 1)$ Prelude.round ((x-xmin)*Prelude.fromIntegral xBins / (xmax - xmin)),
--                   max 0 . min (yBins - 1) $ Prelude.round $ (y-ymin)*Prelude.fromIntegral yBins / (ymax - ymin)),
--                 1)

-- histToImage :: (Pixel a, Num a) => DA.Array (Int, Int) a -> Image a
-- histToImage h = uncurry (generateImage (curry $ ( h !))) $ snd $ bounds h

histToImage :: Pixel e => Array (Int, Int) e -> Image e
histToImage h = uncurry (generateImage (curry $ ( h !))) $ snd $ bounds h

normaliseArray :: DA.Array (Int, Int) Int -> DA.Array (Int, Int) Float
normaliseArray x = ((/ largest) . log . fromIntegral) <$> x
  where
    largest = F.maximum $ (log . fromIntegral <$> x)

main :: IO ()
main = do
  -- renderableToFile def "example.png" chart
  let command = \seed -> someFunc seed . source $ (A.Z A.:. (10000000 :: Int))
  -- let hist = histogram 1000 1000 (-10, 10) (-10,10) . runner $ command
  let hister = runner command
  g <- newStdGen
  let seeds = take 10 $ randoms g
  let hist = Prelude.map (hister . A.fromList A.Z . (:[])) $ seeds
  -- let hist = hists $ Prelude.map (A.fromList (A.Z) . (:[])) [1..10]
  -- print hist
  savePngImage "histoimage.png" . ImageYF . histToImage . normaliseArray . DA.listArray ((0, 0), (999, 999)) . fold1 (zipWith (+)) . map A.toList $ hist
  return ()

fold1 :: (a -> a -> a) -> [a] -> a
fold1 f xs = foldl f (head xs) $ tail xs

addArray :: (Ix i, Num x) => Array i x -> Array i x -> Array i x
addArray a b = DA.listArray (bounds a) $ zipWith (+) (elems a) (elems b)
